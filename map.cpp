#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

/* MAPA:
 * 9x19 == 171c == 85.5 bytes
 */

uint8_t m1[86];
typedef char M2[171];
M2 mapc[50];
const int TAM=4275;
uint8_t map[TAM];

int fl;

int Imp(){int nm,x,y,p=0,p2;char ce;
  fl=open("map.txt",O_RDONLY);if(fl==-1){printf("fallo map.txt\n");return -1;}
  for(nm=0;nm<50;nm++){
    for(x=0;x<9;x++){
      read(fl,mapc[nm]+x*19,19);read(fl,&ce,1);if(ce!=10)printf("ERR-NVL:%d ce\n",nm);
    }read(fl,&ce,1);if(ce!=10)printf("ERR-NVL:%d ce\n",nm);
  }
  close(fl);p2=0;p=0;
  for(nm=0;nm<50;nm++){
    for(y=0;y<19;y++)for(x=0;x<9;x++,p++){p2=x*19+y;
      switch(mapc[nm][p2]){
      case ' ':mapc[nm][p2]=0;break;
      case '@':mapc[nm][p2]=1;break;
      case '1':mapc[nm][p2]=2;break;
      case '*':mapc[nm][p2]=3;break;
      case 'P':mapc[nm][p2]=4;break;
      case '=':mapc[nm][p2]=5;break;
      case '!':mapc[nm][p2]=6;break;
      case 'H':mapc[nm][p2]=7;break;
      case '#':mapc[nm][p2]=8;break;
      case 'O':mapc[nm][p2]=9;break;
      case '$':mapc[nm][p2]=10;break;
      default:printf("NVL:%d - Mal char:%c\n",nm,mapc[nm][p2]);return -1;
      }if(mapc[nm][p2]<0||mapc[nm][p2]>10){printf("NVL:%d - Mal char!\n",nm);return -1;}
      map[p>>1]|=(p&1)?mapc[nm][p2]:(mapc[nm][p2]<<4);
    }
  }
  return 0;
}
int Exp(){int nm,x,y,p=0,p2;
  for(nm=0;nm<50;nm++){
    for(y=0;y<19;y++)for(x=0;x<9;x++,p++){p2=x*19+y;
      mapc[nm][p2]=((p&1)?map[p>>1]:(map[p>>1]>>4))&0xF;
      switch(mapc[nm][p2]){
      case 0:mapc[nm][p2]=' ';break;
      case 1:mapc[nm][p2]='@';break;
      case 2:mapc[nm][p2]='1';break;
      case 3:mapc[nm][p2]='*';break;
      case 4:mapc[nm][p2]='P';break;
      case 5:mapc[nm][p2]='=';break;
      case 6:mapc[nm][p2]='!';break;
      case 7:mapc[nm][p2]='H';break;
      case 8:mapc[nm][p2]='#';break;
      case 9:mapc[nm][p2]='O';break;
      case 10:mapc[nm][p2]='$';break;
      default:printf("%d\n",mapc[nm][p2]);return -1;
      }
    }
  }
  fl=open("map.txt",O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR);
  if(fl==-1){printf("fallo map.txt\n");return -1;}
  for(nm=0;nm<50;nm++){
    for(x=0;x<9;x++){
      write(fl,mapc[nm]+x*19,19);write(fl,"\n",1);
    }write(fl,"\n",1);
  }
  close(fl);
  return 0;
}

int main(int targc,char**argc){
  if(targc!=2){
    printf("%s [Letra]\nE: Exportar. Saca todos los mapas.\nI:Importar. Mete todos los mapas.\n",argc[0]);
    return 0;
  }
  if(argc[1][1]||(argc[1][0]!='E'&&argc[1][0]!='e'&&argc[1][0]!='I'&&argc[1][0]!='i')){
    printf("Mal parametro.\n");return 0;
  }
  if(argc[1][0]=='I'||argc[1][0]=='i')if(Imp())return -1;
  fl=open("fg.exe",O_RDWR);if(fl==-1)return -1;
  lseek(fl,171276,0);
  if(argc[1][0]=='I'||argc[1][0]=='i')write(fl,map,TAM);
    else read(fl,map,TAM);
  close(fl);
  if(argc[1][0]=='E'||argc[1][0]=='e')if(Exp())return -1;
  return 0;
}
