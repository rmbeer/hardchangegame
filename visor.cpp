#include <sys/stat.h>
#include <stdio.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_primitives.h>

ALLEGRO_FONT*font;
ALLEGRO_EVENT_QUEUE*evq;
ALLEGRO_TIMER*timer;

ALLEGRO_DISPLAY*dsp;
ALLEGRO_BITMAP*bmp;
ALLEGRO_COLOR*pal;

long tam,pb,anc,bpf;
char*bf;

#define INC {if(y<anc)y++;else{y=0;x++;}}

int MSK1[8]={1,2,4,8,16,32,64,128};
int MSK2[4]={0,2,4,6};

void print(){long i,j,x=0,y=0;char*bb=bf+pb;
  //Actualiza contenido
  al_set_target_bitmap(bmp);
  //al_draw_filled_rectangle(0,0,anc+3,240,al_map_rgb(0,0,0));
  al_clear_to_color(pal[0]);
  for(i=0;x<240&&pb+i>=0&&pb+i<tam;i++,bb++){
    switch(bpf){
    case 1:
      for(j=0;j<8;j++){
        al_draw_pixel(y,x,pal[(*bb&MSK1[7-j])?15:0]);INC
      }break;
    case 2:
      for(j=0;j<4;j++){
        switch((*bb>>MSK2[3-j])&3){
        case 0:al_draw_pixel(y,x,pal[0]);break;
        case 1:al_draw_pixel(y,x,pal[11]);break;
        case 2:al_draw_pixel(y,x,pal[12]);break;
        case 3:al_draw_pixel(y,x,pal[15]);break;
        }INC
      }break;
    case 4:
      al_draw_pixel(y,x,pal[*bb&15]);INC
      al_draw_pixel(y,x,pal[(*bb>>4)&15]);INC
      break;
    }
  }
  al_draw_textf(font,pal[14],319,0,ALLEGRO_ALIGN_RIGHT,"PB:%8d",pb);
  al_draw_textf(font,pal[14],319,8,ALLEGRO_ALIGN_RIGHT,"ANC:%3d",anc);
  al_draw_textf(font,pal[14],319,16,ALLEGRO_ALIGN_RIGHT,"BP:%1d",bpf);
  //Muestra en pantalla
  al_set_target_backbuffer(dsp);
  al_draw_scaled_bitmap(bmp,0,0,320,240,0,0,640,480,0);
  al_flip_display();
}

int main(int targc,char**argc){
  if(targc!=2){
    printf("%s [Archivo]\nAbre un archivo.\n",argc[0]);
    return 0;
  }
  //Carga contenido archivo
  bf=(char*)malloc(tam);
  if(!bf)return -1;
  {
    int fl=open(argc[1],O_RDONLY);
    if(fl==-1)return -1;
    {struct stat st;
      if(fstat(fl,&st))return -1;
      tam=st.st_size;
    }
    read(fl,bf,tam);
    close(fl);
  }
  //Inicializa allegro
  ALLEGRO_EVENT ev;
  al_init();
  al_install_keyboard();
  al_init_image_addon();
  al_init_primitives_addon();
  al_init_font_addon();
  font=al_create_builtin_font();
  timer=al_create_timer(1.0/30);
  evq=al_create_event_queue();
  al_register_event_source(evq,al_get_timer_event_source(timer));
  al_register_event_source(evq,al_get_keyboard_event_source());
  //CODIGO
  ALLEGRO_COLOR Apal[16]={
    al_map_rgb(0,0,0),al_map_rgb(0,0,192),al_map_rgb(0,192,0),al_map_rgb(0,192,192),
    al_map_rgb(192,0,0),al_map_rgb(192,0,192),al_map_rgb(192,192,0),al_map_rgb(192,192,192),
    al_map_rgb(64,64,64),al_map_rgb(64,64,255),al_map_rgb(64,255,64),al_map_rgb(64,255,255),
    al_map_rgb(255,64,64),al_map_rgb(255,64,255),al_map_rgb(255,255,64),al_map_rgb(255,255,255)
  };pal=Apal;
  dsp=al_create_display(640,480);
  bmp=al_create_bitmap(320,240);
  if(!dsp||!bmp)return -1;
  //al_set_blender(ALLEGRO_ADD,ALLEGRO_ONE,ALLEGRO_ONE);
  al_start_timer(timer);
  al_set_blender(ALLEGRO_ADD,ALLEGRO_ALPHA,ALLEGRO_INVERSE_ALPHA);
  int CICLO=1;
  al_set_target_bitmap(bmp);
  al_clear_to_color(pal[0]);
  anc=7;bpf=1;pb=0;
  //pb=171276;
  print();
  do{
    al_wait_for_event(evq,&ev);
    switch(ev.type){
    case ALLEGRO_EVENT_DISPLAY_CLOSE:CICLO=0;break;
    case ALLEGRO_EVENT_TIMER:break;
    case ALLEGRO_EVENT_KEY_CHAR:
      switch(ev.keyboard.keycode){
      case ALLEGRO_KEY_ESCAPE:CICLO=0;break;
      case ALLEGRO_KEY_PGUP:
        pb-=1024;if(pb<=0)pb=0;
        print();
        break;
      case ALLEGRO_KEY_PGDN:
        if(pb<tam)pb+=1024;
        print();
        break;
      case ALLEGRO_KEY_UP:if(pb>32)pb-=32;print();break;
      case ALLEGRO_KEY_DOWN:if(pb<tam)pb+=32;print();break;
      case ALLEGRO_KEY_LEFT:if(pb>0)pb--;print();break;
      case ALLEGRO_KEY_RIGHT:if(pb<tam)pb++;print();break;
      case ALLEGRO_KEY_PAD_PLUS:if(anc<320)anc++;print();break;
      case ALLEGRO_KEY_PAD_MINUS:if(anc>0)anc--;print();break;
      case ALLEGRO_KEY_1:bpf=1;print();break;
      case ALLEGRO_KEY_2:bpf=2;print();break;
      case ALLEGRO_KEY_4:bpf=4;print();break;
      }break;
    }
  }while(CICLO);
  //Termina allegro
  al_destroy_bitmap(bmp);
  al_destroy_display(dsp);
  al_destroy_event_queue(evq);
  al_destroy_timer(timer);
  al_shutdown_primitives_addon();
  al_shutdown_image_addon();
  al_uninstall_keyboard();
  free(bf);
  return 0;
}
